localStorage.removeItem('results');

let next = document.querySelector('.next');
let previous = document.querySelector('.previous');

let question = document.querySelector('.question');
let answers = document.querySelectorAll('.list-group-item');

let pointsElem = document.querySelector('.score');
let restart = document.querySelector('.restart');

let progressbar = document.querySelector('.progressbar');

let list = document.querySelector('.list');
let results = document.querySelector('.results');
let homepage = document.querySelector('.homepage');
let quiz = document.querySelector('.quiz');

let average = document.querySelector('.average');
let userScorePoint = document.querySelector('.userScorePoint');

let index = 0;
let points = 0;
let answered = [];
let timeLeft = [];
let finish = [];
let preQuestions = [];
let start = false;


fetch('https://quiztai.herokuapp.com/api/quiz')
    .then(resp => resp.json())
    .then(resp => {
        preQuestions = resp;

        for(let i = 0 ; i < preQuestions.length ; i++){
            timeLeft.push( {
                time: 10,
                timeLeft:10
            });
        }

        setInterval(function () {
            if(start) {
                timeLeft[index].timeLeft--;
                progressbar.value = (timeLeft[index].timeLeft * 100) / timeLeft[index].time;

                if (timeLeft[index].timeLeft < 0)
                    disableAnswers();
            }
        },1000);


        setQuestion();
        enableAnswers();

        function doAction(event) {
            //event.target - Zwraca referencję do elementu, do którego zdarzenie zostało pierwotnie wysłane.
            if (event.target.innerHTML === preQuestions[index].correct_answer) {
                points++;
                pointsElem.innerText = points;
                markCorrect(event.target);
            }
            else {
                markInCorrect(event.target);
            }
            preQuestions[index].user_answer=event.target.innerHTML;
            disableAnswers();

        }

        function setQuestion(){
            question.innerHTML = preQuestions[index].question;
            answers[0].innerHTML = preQuestions[index].answers[0];
            answers[1].innerHTML = preQuestions[index].answers[1];
            answers[2].innerHTML = preQuestions[index].answers[2];
            answers[3].innerHTML = preQuestions[index].answers[3];

            if(preQuestions[index].answers.length === 2){
                answers[2].style.display='none';
                answers[3].style.display='none';
            }else{
                answers[2].style.display='block';
                answers[3].style.display='block';
            }
        }


        function disableAnswers() {
            for (let i = 0; i < answers.length; i++) {
                answers[i].removeEventListener('click',doAction);
            }
            for(let i = 0 ; i < answers.length;i++) {
                if (answers[i].classList.contains('correct')) {
                    answers[i].classList.remove('correct');
                } else if (answers[i].classList.contains('incorrect')) {
                    answers[i].classList.remove('incorrect');
                }
            }
            for(let i = 0 ; i < answered.length ; i++){
                if(answered[i].index===index){
                    answers[answered[i].elmIndex].classList.add(answered[i].value);
                }
            }
        }

        function enableAnswers(){
            for (let i = 0; i < answers.length; i++) {
                answers[i].addEventListener('click',doAction);
            }
            for(let i = 0 ; i < answers.length;i++) {
                if (answers[i].classList.contains('correct')) {
                    answers[i].classList.remove('correct');
                } else if (answers[i].classList.contains('incorrect')) {
                    answers[i].classList.remove('incorrect');
                }
            }
        }



        function nextQuestion(){
            let skipQuestion = true;
            if(preQuestions.length>index+1) {
                index++;
                for (let i = 0; i < answered.length; i++) {
                    if (answered[i].index === index ) {
                        skipQuestion = false;
                    }
                }

                if (skipQuestion && timeLeft[index].time-- > 0) {
                    setQuestion();
                    enableAnswers();
                } else{
                    setQuestion();
                    disableAnswers();
                }
            }
            else{
                list.style.display = 'none';
                results.style.display = 'block';
                userScorePoint.innerHTML = points;
                progressbar.style.display='none';
                restart.style.displa = 'block';

                finishQuiz();
            }
        }



        function previousQuestion(){
            let skipQuestion = true;
            restart.style.displa = 'none';
            if(index>0) {
                index--;
                for(let i = 0 ; i < answered.length; i++){
                    if(answered[i].index===index){
                        skipQuestion = false;
                    }
                }

                if(skipQuestion && timeLeft[index].time-- > 0){
                    setQuestion();
                    enableAnswers();

                }else{
                    setQuestion();
                    disableAnswers();
                }

            }
        }


        restart.addEventListener('click', function (event) {
            event.preventDefault();

            index = 0;
            points = 0;
            answered = [];
            finish = [];
            timeLeft = [];

            for(let i = 0 ; i < preQuestions.length ; i++){
                timeLeft.push( {
                    time: 10,
                    timeLeft:10
                });
            }

            progressbar.style.display='block';
            let userScorePoint = document.querySelector('.score');
            userScorePoint.innerHTML = points;
            setQuestion();
            enableAnswers();
            list.style.display = 'block';
            results.style.display = 'none';
        });


        next.addEventListener('click',nextQuestion);
        previous.addEventListener('click',previousQuestion);

    });



function markCorrect(elem) {
    elem.classList.add('correct');
    for (let i = 0; i < answers.length; i++) {
        if (answers[i].classList.contains('correct')) {
            answered.push({
                index: index,
                value: 'correct',
                elmIndex: i
            });
        }
    }
}
function markInCorrect(elem) {
    elem.classList.add('incorrect');
    for (let i = 0; i < answers.length; i++) {
        if (answers[i].classList.contains('incorrect')) {
            answered.push({
                index: index,
                value: 'incorrect',
                elmIndex: i
            });
        }
    }
}

function finishQuiz(){
    if(JSON.parse(localStorage.getItem("results"))===null){
        finish.push({
            amount:1,
            allresults:points,
            average:points

        });
        average.innerHTML = finish[0].average;
        localStorage.setItem('results',JSON.stringify(finish[0]));
        console.log("added new item to local storage");
    }else{
        let lastScore = JSON.parse(localStorage.getItem('results'));
        finish.push({
            amount:lastScore.amount+1,
            allresults:lastScore.allresults+points,
            average:(lastScore.allresults+points)/(lastScore.amount+1)
        });
        average.innerHTML = finish[0].average;
        console.log(finish[0].average);
        localStorage.setItem('results',JSON.stringify(finish[0]));
        console.log("local storage updated");
    }
    console.log(JSON.parse(localStorage.getItem("results")));
}


///////////////////

let startButton = document.querySelector('.start-button');


startButton.addEventListener('click',startFunction);

function startFunction() {
    let r =confirm("While you are solving quiz \nYou wont be able to switch between pages\nDo you want to continue?");
    if(r===true){
        homepage.style.display='none';
        quiz.style.display='block';
        start = true;
    }
}
